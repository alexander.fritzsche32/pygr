#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  6 17:42:54 2023

@author: alexander
"""

from devices import *
from api_access_key import *
from time_trigger import *

import homeassistant_api as ha

import numpy as np
import pandas as pd

from icecream import ic

def logToCsv():
    FILE_PATH = ''

    client = ha.Client(HA_URL,ACCESS_TOKEN)

    device_list = [
        switch(client,'entity_id','switch','room') # placeholder
    ]

    dayTrig = timeTrig()
    minuteTrig = timeTrig()

    while True:
        if dayTrig.Trig(hours=1):
            fileName = 'log_' + nowTimeString() + '.csv'
            df = pd.DataFrame(columns=['time','room','name','data'])
            df.to_csv(ic(FILE_PATH + fileName),index=False)

        if minuteTrig.Trig(minutes=1):
            df = pd.DataFrame({'time':[nowTimeString()],'room':[0],'name':[0],'data':[0]})
       
            for device in device_list:
                df = device.LogData(df)

            df.to_csv(ic(FILE_PATH + fileName),header=False,index=False,mode='a')