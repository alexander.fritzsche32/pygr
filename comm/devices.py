#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  6 14:47:31 2023

@author: alexander
"""

from datetime import datetime

import numpy as np
import pandas as pd

def nowTimeString():
        now = datetime.now()
        return now.strftime('%Y_%m_%d_%H:%M')

class device():
    def __init__(self,client,entityId,friendlyName,room=''):
        self.__client = client
        self.__entityId = entityId
        self.friendlyName = friendlyName
        self.room = room
        self.device_data = None

    def UpdateDevice(self):
        self.device_data = self.__client.get_state(entity_id=self.__entityId)

class switch():
    def __init__(self,client,entityId,friendlyName,room=''):
        self.__switchEntity = device(client,entityId,friendlyName,room)

        self.state = None
        
        self.UpdateDevice()

    def UpdateDevice(self):
        self.__switchEntity.UpdateDevice()

        self.state = self.__switchEntity.device_data.state

    def LogData(self,DataFrame):
        self.UpdateDevice()

        dfAppend = pd.DataFrame(
            {'time':[nowTimeString()],
            'room':[self.__switchEntity.room],
            'name':[self.__switchEntity.friendlyName],
            'data':[{'state':self.state}]}
        )
        DataFrame = pd.concat([DataFrame,dfAppend],ignore_index=True)

        return DataFrame

class thermostat():
    def __init__(self,client,entityId,friendlyName,room=''):
        self.__thermostatEntity = device(client,entityId,friendlyName,room)

        self.measured_temperature = None
        self.set_temperature = None
        self.state = None
        
        self.UpdateDevice()
        
    def UpdateDevice(self):
        self.__thermostatEntity.UpdateDevice()

        self.measured_temperature = self.__thermostatEntity.device_data.attributes['current_temperature']
        self.set_temperature = self.__thermostatEntity.device_data.attributes['temperature']
        self.state = self.__thermostatEntity.device_data.state

    def LogData(self,DataFrame):
        self.UpdateDevice()
        
        dfAppend = pd.DataFrame(
            {'time':[nowTimeString()],
            'room':[self.__thermostatEntity.room],
            'name':[self.__thermostatEntity.friendlyName],
            'data':[
            {'measured_temperature':self.measured_temperature,
            'set_temperature':self.set_temperature,
            'state':self.state}]}
        )
        DataFrame = pd.concat([DataFrame,dfAppend],ignore_index=True)

        return DataFrame

class blinds():
    def __init__(self,client,entityId,friendlyName,orientation,room=''):
        self.__blindsEntity = device(client,entityId,friendlyName,room)

        self.__position = None
        self.__state = None
        self.__orientation = orientation
        
        self.UpdateDevice()

    def UpdateDevice(self):
        self.__blindsEntity.UpdateDevice()

        self.__position = self.__blindsEntity.device_data.attributes['current_position']
        self.__state = self.__blindsEntity.device_data.state

    def LogData(self,DataFrame):
        self.UpdateDevice()
        
        dfAppend = pd.DataFrame(
            {'time':[nowTimeString()],
            'room':[self.__blindsEntity.room],
            'name':[self.__blindsEntity.friendlyName],
            'data':[
            {'orientation':self.__orientation,
            'position':self.__position,
            'state':self.__state}]}
        )
        DataFrame = pd.concat([DataFrame,dfAppend],ignore_index=True)

        return DataFrame

class switchPowerMeasurement():
    def __init__(self,client,switchEntityId,powerEntityId,friendlyName,room=''):
        self.__switchEntity = device(client,switchEntityId,friendlyName,room)
        self.__powerEntity = device(client,powerEntityId,friendlyName,room)

        self.__state = None
        self.__power = None
        
        self.UpdateDevice()

    def UpdateDevice(self):
        self.__switchEntity.UpdateDevice()
        self.__powerEntity.UpdateDevice()  

        self.__state = self.__switchEntity.device_data.state
        self.__power = self.__powerEntity.device_data.state
    
    def LogData(self,DataFrame):
        self.UpdateDevice()
        
        dfAppend = pd.DataFrame(
            {'time':[nowTimeString()],
            'room':[self.__switchEntity.room],
            'name':[self.__switchEntity.friendlyName],
            'data':[
            {'power':self.__power,
            'state':self.__state}]}
        )
        DataFrame = pd.concat([DataFrame,dfAppend],ignore_index=True)

        return DataFrame

class solarPower():
    def __init__(self,client,generatedPowerEntityId,powerToNetEntityId,friendlyName,room=''):
        self.__generatedPowerEntity = device(client,generatedPowerEntityId,friendlyName,room)
        self.__powerToNetEntity = device(client,powerToNetEntityId,friendlyName,room)

        self.__generatedPower = None
        self.__powerToNet = None
        self.__powerToHome = None
        
        self.UpdateDevice()

    def UpdateDevice(self):
        self.__generatedPowerEntity.UpdateDevice()
        self.__powerToNetEntity.UpdateDevice()  

        self.__generatedPower = self.__generatedPowerEntity.device_data.state
        self.__powerToNet = self.__powerToNetEntity.device_data.state
        self.__powerToHome = str(int(self.__generatedPower) - int(self.__powerToNet))

    def LogData(self,DataFrame):
        self.UpdateDevice()
        
        dfAppend = pd.DataFrame(
            {'time':[nowTimeString()],
            'room':[self.__generatedPowerEntity.room],
            'name':[self.__generatedPowerEntity.friendlyName],
            'data':[
            {'generated_power':self.__generatedPower,
            'power_to_net':self.__powerToNet,
            'power_to_home':self.__powerToHome}]}
        )
        DataFrame = pd.concat([DataFrame,dfAppend],ignore_index=True)
       
        return DataFrame