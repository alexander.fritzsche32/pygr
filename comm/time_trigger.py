#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  4 21:42:11 2023

@author: alexander
"""

from datetime import datetime,timedelta

class timeTrig:
    def __init__(self):
        self.__datetime = None
        self.__UpdateTime()
        self.__datetimeBuffer = self.__datetime
        self.__startup = True
        
    def __UpdateTime(self):
       self.__datetime = datetime.now().replace(microsecond=0)
    
    def Trig(self,weeks=0,days=0,hours=0,minutes=0,seconds=0):
        self.__UpdateTime()

        weeks,days,hours,minutes,seconds = self.__ensureMinZero(weeks,days,hours,minutes,seconds)

        if self.__datetime == timedelta(weeks=weeks,days=days,hours=hours,minutes=minutes,seconds=seconds) + self.__datetimeBuffer:
            self.__datetimeBuffer = self.__datetime
            
            return True
        elif self.__startup:
            self.__startup = False
            
            return True
        else:
            return False

    def __ensureMinZero(self,*args):
        for arg in args:
            if arg < 0:
                arg = 0
        
        return args