from fastai.imports import *
np.set_printoptions(linewidth=130)

from path_constant import DATA_PATH,MACHINE_LEARNING_PATH
from data_base_functions import *

from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error
from sklearn.ensemble import RandomForestClassifier
import joblib

import datetime

from icecream import ic

def loadData(count):
    filePath = (
        DATA_PATH +
        'split_room_day_schedule_' +
        str(count) + 
        '.csv'
    )
    dataFrame = pd.read_csv(filePath,delimiter=',')

    return dataFrame

def saveRandomForrest(randomForrest,count):
    filePath = (
            MACHINE_LEARNING_PATH +
            'random_forrest_' +
            str(count) + 
            '.joblib'
        )
    joblib.dump(randomForrest,filePath)

    print(filePath)

def xs_y(dataFrame):
    continuous = [
        'type',
        'month',
        'day',
        'time',
        'temperature_2m',
        'cloudcover',
        'is_day',
        'solar power',
        'used power',
        'last at home',
        'presence',
        'last at room',
        'temperature room'
    ]

    dependent = 'heating room'
    
    xs = dataFrame[continuous].copy()
    
    return xs,dataFrame[dependent] if dependent in dataFrame else None

def randomForrest(count):
    

    for i in range(count):
        dfData = loadData(i)

        dfTraining,dfValidation = train_test_split(dfData,test_size=0.25)
        del dfData
        print('Dataset split done')

        xsTraining,yTraining = xs_y(dfTraining)
        xsValidation,yValidation = xs_y(dfValidation)
        ic(xsTraining.head())
        ic(yTraining.head())
        ic(xsValidation.head())
        ic(yValidation.head())
        del dfTraining,dfValidation
        print('Variable and dependent split')

        randomForrest = RandomForestClassifier(100,min_samples_leaf=5)
        print('Classifier created, beginning fitting ...')
        
        randomForrest.fit(xsTraining,yTraining)

        ic(mean_absolute_error(yValidation,randomForrest.predict(xsValidation)))

        saveRandomForrest(randomForrest,i)