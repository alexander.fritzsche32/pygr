#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  9 21:39:18 2023

@author: alexander
"""

import sys

PROJECT_PATH = '~/Documents/code/python/pygr'
DATA_PATH = '~/Documents/code/python/pygr/data/'
GENERATOR_PATH = '~/Documents/code/python/pygr/generator/'
MACHINE_LEARNING_PATH = '~/Documents/code/python/pygr/machine_learning'
COMMUNINCATION_PATH = '~/Documents/code/python/pygr/comm'

def addPaths():
    sys.path.append(PROJECT_PATH)
    sys.path.append(DATA_PATH)
    sys.path.append(GENERATOR_PATH)
    sys.path.append(MACHINE_LEARNING_PATH)
    sys.path.append(COMMUNINCATION_PATH)