import path_constant as pc

pc.addPaths()

from init_day_type import initDayType
from vacation_casino import vacationCasino
from sick_day_casino import sickDayCasino
from day_schedule_casino import dayScheduleCasino
from day_schedule_presence import presenceDataSingleThread
from combine_weather_data import combineWeatherData
from heating_casino import heatingCasino
from split_rooms import splitRooms
from random_forrest import randomForrest

import datetime

from icecream import ic

if __name__ == '__main__':
    startTime = datetime.datetime.now()
    start = 'start ' + str(startTime)
    ic(start)

    # initDayType()
    # vacationCasino(10)
    # sickDayCasino(10)
    # dayScheduleCasino(10)
    # presenceDataSingleThread(10)
    # combineWeatherData(10)
    # heatingCasino(10)
    # splitRooms(10)
    randomForrest(1)

    finishTime = datetime.datetime.now()
    finish = 'finish ' + str(finishTime)
    ic(finish)

    delta = 'process time ' + str(finishTime - startTime)
    ic(delta)