#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 11 15:51:55 2023

@author: alexander
"""

from path_constant import DATA_PATH

import datetime
import random

import numpy as np
import pandas as pd

def loadData(count):
    filePath = (
        DATA_PATH +
        'date_type_with_vacation_2000_01_to_2023_08_' +
        str(count) +
        '.csv'
    )
    dataFrame = pd.read_csv(filePath,delimiter=',')

    return dataFrame

def writeData(dataFrame,count):
    filePath = (
        DATA_PATH + 
        'date_type_with_vacation_sick_days_2000_01_to_2023_08_' + 
        str(count) +
        '.csv'
    )
    dataFrame.to_csv(filePath,index=False)

def sickDayCasino(count):
    for i in range(count):
        dfDate = loadData(i)

        index = 0
        lastYear = 2000

        while index in range(len(dfDate['date'])):
            indexIterator = setSickDays(dfDate,index)

            index = index + indexIterator

        writeData(dfDate,i)

def setSickDays(dataFrame,index):
    if random.randrange(1,1000) <= 2:
        sickDuration = random.randrange(3,14)

        for i in range(index,index + sickDuration):
            dataFrame.at[i,'type'] = 'sick'

        return sickDuration
    else:
        return 1