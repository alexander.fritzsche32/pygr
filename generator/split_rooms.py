from path_constant import DATA_PATH
from data_base_functions import *

import datetime
import random

import numpy as np
import pandas as pd

from icecream import ic

def loadData(count):
    filePath = (
        DATA_PATH +
        'presence_heating_day_schedule_' +
        str(count) + 
        '.csv'
    )
    dataFrame = pd.read_csv(
        filePath,
        delimiter=',')

    print(filePath)

    return dataFrame

def writeData(dataFrame,count):
    filePath = (
        DATA_PATH +
        'split_room_day_schedule_' +
        str(count) + 
        '.csv'
    )
    dataFrame.to_csv(filePath,index=False)

    print(filePath)

def splitRooms(count):
    for i in range(count):
        inputDataFrame = loadData(i)

        generalColumns = [
            'month',
            'day',
            'time', 
            'temperature_2m',  
            'cloudcover',          
            'is_day', 
            'solar power', 
            'used power',
            'last at home',
            'presence',
            'last at room', 
            'temperature room', 
            'heating room'
        ]

        bathroomColumns = [
            'month',
            'day',
            'time', 
            'temperature_2m',  
            'cloudcover',          
            'is_day', 
            'solar power', 
            'used power',
            'last at home',
            'bathroom',
            'last at bathroom', 
            'temperature bathroom', 
            'heating bathroom'
        ]

        bedroomColumns = [
            'month',
            'day',
            'time', 
            'temperature_2m',  
            'cloudcover',          
            'is_day', 
            'solar power', 
            'used power',
            'last at home',
            'bedroom',
            'last at bedroom', 
            'temperature bedroom', 
            'heating bedroom'
        ]

        kitchenColumns = [
            'month',
            'day',
            'time', 
            'temperature_2m',  
            'cloudcover',          
            'is_day', 
            'solar power', 
            'used power',
            'last at home',
            'kitchen', 
            'last at kitchen', 
            'temperature kitchen', 
            'heating kitchen'
        ]

        livingRoomColumns = [
            'month',
            'day',
            'time', 
            'temperature_2m',  
            'cloudcover',          
            'is_day', 
            'solar power', 
            'used power',
            'last at home',
            'living room',
            'last at living room', 
            'temperature living room', 
            'heating living room'
        ]

        studyColumns = [
            'month',
            'day',
            'time', 
            'temperature_2m',  
            'cloudcover',          
            'is_day', 
            'solar power', 
            'used power',
            'last at home',
            'study',
            'last at study', 
            'temperature study', 
            'heating study'
        ]

        roomColumns = [
            bathroomColumns,
            bedroomColumns,
            kitchenColumns,
            livingRoomColumns,
            studyColumns
        ]
        
        outputColumns = generalColumns
        outputColumns.append('type')

        outputDataFrame = pd.DataFrame(columns=outputColumns)

        day = 0
        debug = 0

        for j in range(len(inputDataFrame['date'])):
            if debug == 0:
                startTime = datetime.datetime.now()

            inputDataFrame.at[j,'month'],inputDataFrame.at[j,'day'] = dateStringToInteger(inputDataFrame['date'].iloc[j])
            inputDataFrame.at[j,'time'] = np.log1p(timeStringToInteger(inputDataFrame['time'].iloc[j]))
            inputDataFrame.at[j,'solar power'] = np.log1p(inputDataFrame['solar power'].iloc[j])
            inputDataFrame.at[j,'used power'] = np.log1p(inputDataFrame['used power'].iloc[j])
            inputDataFrame.at[j,'last at home'] = np.log1p(inputDataFrame['last at home'].iloc[j])
            inputDataFrame.at[j,'last at bathroom'] = np.log1p(inputDataFrame['last at bathroom'].iloc[j])
            inputDataFrame.at[j,'last at bedroom'] = np.log1p(inputDataFrame['last at bedroom'].iloc[j])
            inputDataFrame.at[j,'last at kitchen'] = np.log1p(inputDataFrame['last at kitchen'].iloc[j])
            inputDataFrame.at[j,'last at living room'] = np.log1p(inputDataFrame['last at living room'].iloc[j])
            inputDataFrame.at[j,'last at study'] = np.log1p(inputDataFrame['last at study'].iloc[j])

            debug = debug + 1

            if debug == 1440:
                debug = 0
                endTime = datetime.datetime.now()

                print('task ' + str(day) + ' done ' + str(endTime - startTime))

                day = day + 1

        for j in range(5):
            roomDataFrame = pd.DataFrame(columns=generalColumns)

            for k in range(13):
                roomDataFrame[generalColumns[k]] = inputDataFrame[roomColumns[j][k]]

            roomDataFrame['type'] = j

            if j == 0:
                outputDataFrame = roomDataFrame
            else:
                outputDataFrame = pd.concat([outputDataFrame,roomDataFrame],axis=0)

        writeData(outputDataFrame,i)

def dateStringToInteger(dateString):
    month = int(dateString[5:7])
    day = int(dateString[8:])

    return month,day

def timeStringToInteger(timeString):
    hours = int(timeString[:2])
    minutes = int(timeString[3:])

    return hours * 60 + minutes