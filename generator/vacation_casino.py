#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 11 15:41:55 2023

@author: alexander
"""

from path_constant import DATA_PATH
from data_base_functions import *

import datetime
import random

import numpy as np
import pandas as pd

def loadData():
    dataFrame = pd.read_csv(
        DATA_PATH + 
        'date_type_2000_01_to_2023_08.csv',
        delimiter=',')

    return dataFrame

def writeData(dataFrame,count):
    filePath = (
        DATA_PATH +
        'date_type_with_vacation_2000_01_to_2023_08_' +
        str(count) +
        '.csv')
    dataFrame.to_csv(filePath,index=False)
        
def vacationCasino(count):
    for i in range(count):
        dfDate = loadData()

        index = 0
        longVacationCount = 0
        shortVacationCount = 0
        lastYear = 2000

        while index in range(len(dfDate['date'])):
            date = stringToDatetime(dfDate.at[index,'date'])

            if date.year != lastYear:
                longVacationCount = 0
                shortVacationCount = 0
                lastYear = date.year

            if (date.weekday() == 0 and
            (date.month in range(4) and shortVacationCount == 0 or
            date.month in range(4,7) and shortVacationCount <= 1 or
            date.month in range(10,13) and shortVacationCount <= 2)):
                setTypeVacationShort(date,dfDate,index)

                shortVacationCount = shortVacationCount + 1            
                index = index + 7
            elif (date.weekday() == 0 and
                date.month in range(7,10) and 
                longVacationCount == 0):
                    setTypeVacationLong(date,dfDate,index)
                
                    longVacationCount = longVacationCount + 1
                    index = index + 14
            elif dateIsHoliday(dfDate,index) and  index != 0:
                setTypeFenstertag(date,dfDate,index)

                index = index + 1
            else:
                index = index + 1

        writeData(dfDate,i)

def setTypeVacationShort(date,dataFrame,index):
    if random.randrange(1,3) <= 1:
        for i in range(index,index + 5):
            if dateIsWork(dataFrame,i):
                dataFrame.at[i,'type'] = 'vacation'

def setTypeVacationLong(date,dataFrame,index):
    if random.randrange(1,3) <= 1:
        for i in range(index,index + 12):
            if dateIsWork(dataFrame,i):
                dataFrame.at[i,'type'] = 'vacation'

def setTypeFenstertag(date,dataFrame,index):
    if  date.weekday() == 1:
        dataFrame.at[index - 1,'type'] = 'vacation'
    elif date.weekday() == 3:
        dataFrame.at[index + 1,'type'] = 'vacation'