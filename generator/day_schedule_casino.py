#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  2 17:26:00 2023

@author: alexander
"""

from path_constant import DATA_PATH
from data_base_functions import *

import datetime
import random

import numpy as np
import pandas as pd

def loadData(count):
    filePath = (
        DATA_PATH +
        'date_type_with_vacation_sick_days_2000_01_to_2023_08_' +
        str(count) + 
        '.csv'
    )
    dataFrame = pd.read_csv(
        filePath,
        delimiter=',')

    print(filePath)

    return dataFrame

def writeData(dataFrame,count):
    filePath = (
        DATA_PATH +
        'day_schedule_2000_01_to_2023_08_' +
        str(count) + 
        '.csv'
    )
    dataFrame.to_csv(filePath,index=False)

    print(filePath)

def dayScheduleCasino(count):
    columns = [
        'bathroom',
        'bedroom',
        'kitchen',
        'living room',
        'study'
    ]

    dfAdd = pd.DataFrame(columns=columns)

    for i in range(count):
        dfDate = loadData(i)
        dfDate = dfDate.join(dfAdd)

        vacationType = 0
        vacationLength = 0

        for index in range(len(dfDate['date'])):
            if dateIsWork(dfDate,index):
                vacationType = 0
                vacationLength = 0
                workDaySchedule(dfDate,index)
            elif dateIsSick(dfDate,index):
                vacationType = 0
                vacationLength = 0
                sickDaySchedule(dfDate,index)
            elif dateIsFree(dfDate,index):
                vacactionFirstDay = False
                vacationLastDay = False

                if not dateIsFree(dfDate,index - 1):
                    vacactionFirstDay = True
                
                if not dateIsFree(dfDate,index + 1):
                    vacationLastDay = True

                if vacationLength == 0: 
                    while dateIsFree(dfDate,index + vacationLength):
                        vacationLength = vacationLength + 1
                
                if vacationType == 0:
                    lateReturn = False

                    if vacationLength >= 5 or (vacationLength >= 3 and random.randrange(0,3) == 0):
                        vacationType = 2
                    else:
                        vacationType = 1
                    
                if vacationType == 1 or (vacactionFirstDay and vacationLastDay):
                    lateReturn = staycationSchedule(dfDate,index,vacationLastDay,lateReturn)
                elif vacationType == 2:
                    vacationScedule(dfDate,index,vacactionFirstDay,vacationLastDay)

        writeData(dfDate,i)

def workDaySchedule(dataFrame,index,workHours = [(8,30),(17,30)]):
    date = stringToDatetime(dataFrame.at[index,'date'])

    leaveTime = randomizeTime(workHours[0],(-7,16))
    returnTime = randomizeTime(workHours[1],(-15,56))

    morningKitchenEnterTime = randomizeTime((7,0),(-5,16))
    eveningKitchenEnterTime = randomizeTime((20,20),(-30,60))
    eveningKitchenExitTime = randomizeTime(eveningKitchenEnterTime,(30,46))

    morningBathroomEnterTime = randomizeTime(workHours[0],(-35,-29))
    morningBathroomExitTime = randomizeTime(morningBathroomEnterTime,(15,21))
    eveningBathroomEnterTime = randomizeTime(eveningKitchenExitTime,(20,56))
    eveningBathroomExitTime = randomizeTime(eveningBathroomEnterTime,(15,21))

    dataFrame.at[index,'bathroom'] = [
        (morningBathroomEnterTime,morningBathroomExitTime),
        (eveningBathroomEnterTime,eveningBathroomExitTime)
    ]

    dataFrame.at[index,'bedroom'] = [
        ((0,0),morningKitchenEnterTime),
        (morningBathroomExitTime,leaveTime),
        (eveningBathroomExitTime,(23,59))
    ]
    
    dataFrame.at[index,'kitchen'] = [
        (morningKitchenEnterTime,morningBathroomEnterTime),
        (eveningKitchenEnterTime,eveningKitchenExitTime)
    ]
    
    if date.weekday() == 4 and random.randrange(0,2) == 0:
        returnTime = randomizeTime((22,59),(-75,1))
        
        eveningBathroomExitTime = randomizeTime(returnTime,(15,20))

        dataFrame.at[index,'bathroom'] = [
            (morningBathroomEnterTime,morningBathroomExitTime),
            (returnTime,eveningBathroomExitTime)
        ]

        dataFrame.at[index,'bedroom'] = [
            ((0,0),morningKitchenEnterTime),
            (morningBathroomExitTime,leaveTime),
            (eveningBathroomExitTime,(23,59))
        ]

        dataFrame.at[index,'kitchen'] = [
            (morningKitchenEnterTime,morningBathroomEnterTime)
        ]

    elif random.randrange(0,4) == 0:
        studyExitTime = randomizeTime(eveningKitchenEnterTime,(-30,-13))

        dataFrame.at[index,'study'] = [
            (returnTime,studyExitTime)
        ]
        
        dataFrame.at[index,'living room'] = [
            (studyExitTime,eveningKitchenEnterTime),
            (eveningKitchenExitTime,eveningBathroomEnterTime)
        ]
    else:
        dataFrame.at[index,'living room'] = [
            (returnTime,eveningKitchenEnterTime),
            (eveningKitchenExitTime,eveningBathroomEnterTime)
        ]

def sickDaySchedule(dataFrame,index):
    morningKitchenEnterTime = randomizeTime((9,0),(-5,16))
    morningKitchenExitTime = randomizeTime(morningKitchenEnterTime,(10,15))
    eveningKitchenEnterTime = randomizeTime((20,20),(-30,60))
    eveningKitchenExitTime = randomizeTime(eveningKitchenEnterTime,(10,15))

    dataFrame.at[index,'bedroom'] = [
        ((0,0),morningKitchenEnterTime),
        (morningKitchenExitTime,eveningKitchenEnterTime),
        (eveningKitchenExitTime,(23,59))
    ]
    
    dataFrame.at[index,'kitchen'] = [
        (morningKitchenEnterTime,morningKitchenExitTime),
        (eveningKitchenEnterTime,eveningKitchenExitTime)
    ]

def staycationSchedule(dataFrame,index,vacationLastDay,lateReturn):
    if lateReturn:
        morningBedroomEnterTime = randomizeTime((0,0),(0,180))
    else:
        morningBedroomEnterTime = (0,0)

    lateReturn = False
    
    lunchKitchenEnterTime = randomizeTime((14,30),(-45,60))
    lunchKitchenExitTime = randomizeTime(lunchKitchenEnterTime,(35,90))
    eveningKitchenEnterTime = randomizeTime((20,20),(-30,60))
    eveningKitchenExitTime = randomizeTime(eveningKitchenEnterTime,(30,46))

    morningBathroomEnterTime = randomizeTime(morningKitchenEnterTime,(30,60))
    morningBathroomExitTime = randomizeTime(morningBathroomEnterTime,(15,21))
    eveningBathroomEnterTime = randomizeTime(eveningKitchenExitTime,(20,56))
    eveningBathroomExitTime = randomizeTime(eveningBathroomEnterTime,(5,16))
    
    morningBedroomExitTime = randomizeTime(morningBathroomExitTime,(5,21))

    morningStudyEnterTime = randomizeTime(morningBedroomExitTime,(20,35))
    morningStudyExitTime = randomizeTime(lunchKitchenEnterTime,(-35,-5))
    eveningStudyEnterTime = randomizeTime(lunchKitchenExitTime,(5,35))
    eveningStudyExitTime = randomizeTime(eveningKitchenEnterTime,(-35,-5))

    dayReturnTime = randomizeTime(eveningKitchenEnterTime,(-35,-5))

    eveningReturnTime = randomizeTime((22,59),(-25,1))

    dataFrame.at[index,'bathroom'] = [
        (morningBathroomEnterTime,morningBathroomExitTime),
        (eveningBathroomEnterTime,eveningBathroomExitTime)
    ]

    dataFrame.at[index,'bedroom'] = [
        (morningBedroomEnterTime,morningKitchenEnterTime),
        (morningBathroomExitTime,morningBedroomExitTime),
        (eveningBathroomExitTime,(23,59))
    ]
    
    dataFrame.at[index,'kitchen'] = [
        (morningKitchenEnterTime,morningBathroomEnterTime),
        (lunchKitchenEnterTime,lunchKitchenExitTime),
        (eveningKitchenEnterTime,eveningKitchenExitTime)
    ]

    if random.randrange(0,3) == 0: # stay home with study
        studyActivity = random.randrange(0,3)

        if studyActivity == 0: # morning only
            dataFrame.at[index,'study'] = [
                (morningStudyEnterTime,morningStudyExitTime)
            ]

            dataFrame.at[index,'living room'] = [
                (morningBedroomExitTime,morningStudyEnterTime),
                (morningStudyExitTime,lunchKitchenEnterTime),
                (lunchKitchenExitTime,eveningKitchenEnterTime),
                (eveningKitchenExitTime,eveningBathroomEnterTime)
            ]
        elif studyActivity == 1: # evening only
            dataFrame.at[index,'study'] = [
                (eveningStudyEnterTime,eveningStudyExitTime)
            ]

            dataFrame.at[index,'living room'] = [
                (morningBedroomExitTime,lunchKitchenEnterTime),
                (lunchKitchenExitTime,eveningStudyEnterTime),
                (eveningStudyExitTime,eveningKitchenEnterTime),
                (eveningKitchenExitTime,eveningBathroomEnterTime)
            ]
        elif studyActivity == 2: # both
            dataFrame.at[index,'study'] = [
                (morningStudyEnterTime,morningStudyExitTime),
                (eveningStudyEnterTime,eveningStudyExitTime)
            ]

            dataFrame.at[index,'living room'] = [
                (morningBedroomExitTime,morningStudyEnterTime),
                (morningStudyExitTime,lunchKitchenEnterTime),
                (lunchKitchenExitTime,eveningStudyEnterTime),
                (eveningStudyExitTime,eveningKitchenEnterTime),
                (eveningKitchenExitTime,eveningBathroomEnterTime)
            ]
    elif random.randrange(0,2) == 0: # day activity
        dataFrame.at[index,'kitchen'] = [
            (morningKitchenEnterTime,morningBathroomEnterTime),
            (eveningKitchenEnterTime,eveningKitchenExitTime)
        ]

        dataFrame.at[index,'living room'] = [
            (dayReturnTime,eveningKitchenEnterTime),
            (eveningKitchenExitTime,eveningBathroomEnterTime)
        ]
    else: # stay home no study
        dataFrame.at[index,'living room'] = [
            (morningBedroomExitTime,lunchKitchenEnterTime),
            (lunchKitchenExitTime,eveningKitchenEnterTime),
            (eveningKitchenExitTime,eveningBathroomEnterTime)
        ]
    
    if random.randrange(0,2) == 0: # evening activity
        if random.randrange(0,2) == 0 and not vacationLastDay: # late Return
            lateReturn = True

            dataFrame.at[index,'bathroom'] = [
                (morningBathroomEnterTime,morningBathroomExitTime)
            ]

            dataFrame.at[index,'bedroom'] = [
                (morningBedroomEnterTime,morningKitchenEnterTime),
                (morningBathroomExitTime,morningBedroomExitTime)
            ]
        else:
            eveningBathroomExitTime = randomizeTime(eveningReturnTime,(10,15))

            dataFrame.at[index,'bathroom'] = [
                (morningBathroomEnterTime,morningBathroomExitTime),
                (eveningReturnTime,eveningBathroomExitTime)
            ]

            dataFrame.at[index,'bedroom'] = [
                (morningBedroomEnterTime,morningKitchenEnterTime),
                (morningBathroomExitTime,morningBedroomExitTime),
                (eveningBathroomExitTime,(23,59))
            ]

            dataFrame.at[index,'living room'].pop()
            dataFrame.at[index,'kitchen'].pop()

    return lateReturn

def vacationScedule(dataFrame,index,vacactionFirstDay,vacationLastDay):
    leaveTime = randomizeTime((9,0),(-180,180))
    returnTime = randomizeTime((15,0),(-60,240))

    morningKitchenEnterTime = randomizeTime(leaveTime,(-120,-90))
    morningKitchenExitTime = randomizeTime(morningKitchenEnterTime,(25,40))
    eveningKitchenEnterTime = randomizeTime((20,20),(-30,60))
    eveningKitchenExitTime = randomizeTime(eveningKitchenEnterTime,(30,46))

    morningBathroomEnterTime = morningKitchenExitTime
    morningBathroomExitTime = randomizeTime(morningBathroomEnterTime,(15,21))
    eveningBathroomEnterTime = eveningKitchenExitTime
    eveningBathroomExitTime = randomizeTime(eveningBathroomEnterTime,(15,21))

    if vacactionFirstDay:
        dataFrame.at[index,'bathroom'] = [
            (morningBathroomEnterTime,morningBathroomExitTime)
        ]

        dataFrame.at[index,'bedroom'] = [
            ((0,0),morningKitchenEnterTime),
            (morningBathroomExitTime,leaveTime)
        ]
        
        dataFrame.at[index,'kitchen'] = [
            (morningKitchenEnterTime,morningBathroomEnterTime)
        ]
    elif vacationLastDay:
        dataFrame.at[index,'bathroom'] = [
            (eveningBathroomEnterTime,eveningBathroomExitTime)
        ]

        dataFrame.at[index,'bedroom'] = [
            (eveningBathroomExitTime,(23,59))
        ]
        
        dataFrame.at[index,'kitchen'] = [
            (eveningKitchenEnterTime,eveningKitchenExitTime)
        ]

def randomizeTime(time = (0,0),deviation = (-5,6)):
    hours = time[0]
    minutes = time[1] + random.randrange(deviation[0],deviation[1])

    while minutes > 59:
        minutes = minutes - 59
        hours = hours + 1

        if hours > 23:
            hours = 0

    while minutes < 0:
        minutes = minutes + 60
        hours = hours - 1

        if hours < 0:
            hours = 23

    return (hours,minutes)