#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  7 14:43:22 2023

@author: alexander
"""

from path_constant import DATA_PATH

import datetime
import holidays

import numpy as np
import pandas as pd

def initDayType():
    startDate = datetime.date(2000, 1, 1)
    endDate = datetime.date(2023, 8, 31)

    dayStep = datetime.timedelta(days=1)

    currentDate = startDate

    dfDate = pd.DataFrame(columns=['date','type'])

    holiday = holidays.Austria(years=range(2000,2024))

    while currentDate <= endDate:
        if currentDate in holiday:
            dateType = 'holiday'
        elif currentDate.weekday() < 5:
            dateType = 'work'
        else :
            dateType = 'off'
        
        entry = pd.DataFrame({'date':[currentDate],'type':[dateType]})
        dfDate = pd.concat([dfDate,entry],ignore_index=True)
        
        currentDate = currentDate + dayStep

    dfDate.to_csv(DATA_PATH + 'date_type_2000_01_to_2023_08.csv',index=False)