#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 17:53:00 2023

@author: alexander
"""

from path_constant import DATA_PATH

import datetime

import numpy as np
import pandas as pd

import ast
from icecream import ic
import threading

class threadTask():
    def __init__(self,dataFrame):
        self.result = []
        self.dataFrame = dataFrame
    
    def Run(self,days,columns):
        for day in range(days[0],days[1]):
            self.result.append(createContinuousData(self.dataFrame.iloc[day],day,columns))

def loadData(count):
    filePath = (
        DATA_PATH +
        'day_schedule_2000_01_to_2023_08_' +
        str(count) + 
        '.csv'
    )
    dataFrame = pd.read_csv(
        filePath,
        delimiter=',')

    columns = dataFrame.columns

    for column in columns[2:]:
        dataFrame[column] = dataFrame[column].apply(stringToData)

    return dataFrame

def writeData(results,count):

    filePath = (
        DATA_PATH +
        'presence_day_schedule_' +
        str(count) +
        '.csv'
    )

    dfData = pd.concat(results,ignore_index=True)

    dfData.to_csv(filePath,index=False)

def presenceDataSingleThread(count):
    for i in range(count,count+1):
        results = []
    
        dfDate = loadData(i)

        columnsTemp = dfDate.columns
        columns = ['date','time']
        columns.extend(columnsTemp[2:])

        for day in range(len(dfDate['date'])):
            results.append(createContinuousData(dfDate.iloc[day],day,columns))

        writeData(results,i)

def createContinuousData(dataFrameRow,day,columns):
    startTime = datetime.datetime.now()

    MINUTES_PER_DAY = 60 * 24

    date = dataFrameRow['date']

    dfDay = pd.DataFrame(columns=columns)
    
    for column in columns:
        for i in range(MINUTES_PER_DAY):
            if column == 'date':
                dfDay.at[i,column] = date
            elif column == 'time':
                dfDay.at[i,column] = timeTupleToString(sumOfMinutesToTime(i))
            else:
                dfDay.at[i,column] = np.nan

    for column in columns[2:]:
        timeList = dataFrameRow[column]

        if timeList != []:
            for entry in timeList:
                dfDay.at[timeToSumOfMinutes(entry[0]),column] = 1

                if timeToSumOfMinutes(entry[1]) == 1439:
                    dfDay.at[timeToSumOfMinutes(entry[1]),column] = 1
                else:
                    dfDay.at[timeToSumOfMinutes(entry[1]),column] = 0

        data = 0

        for i in range(MINUTES_PER_DAY):
            if np.isnan(dfDay.at[i,column]):
                dfDay.at[i,column] = data
            else:
                data = dfDay.at[i,column]

    endTime = datetime.datetime.now()

    print('task ' + str(day) + ' done ' + str(endTime - startTime))

    return dfDay

def timeToSumOfMinutes(time=(0,0)):
    return time[0] * 60 + time[1]

def sumOfMinutesToTime(minutes = 0):
    hours = 0

    while minutes > 59:
        hours = hours + 1
        minutes = minutes - 60

    return (hours,minutes)

def timeTupleToString(time=(0,0)):
    hours = str(time[0])

    if time[0] < 10:
        hours = '0' + hours

    minutes = str(time[1])

    if time[1] < 10:
        minutes = '0' + minutes

    return hours + ':' + minutes

def stringToData(input):
    try:
        return ast.literal_eval(input)
    except (SyntaxError,ValueError):
        return []