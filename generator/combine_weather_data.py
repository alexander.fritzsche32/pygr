#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 17:53:00 2023

@author: alexander
"""

from path_constant import DATA_PATH
from data_base_functions import *

import datetime
import random

import numpy as np
import pandas as pd

from icecream import ic
import matplotlib.pyplot as plt

def loadWeatherData():
    filePath = (
        DATA_PATH +
        'weather_data_clean.csv'
    )
    dataFrame = pd.read_csv(
        filePath,
        delimiter=',')

    print(filePath)

    return dataFrame

def loadPresenceData(count):
    filePath = (
        DATA_PATH +
        'presence_day_schedule_' +
        str(count) +
        '.csv'
    )
    dataFrame = pd.read_csv(
        filePath,
        delimiter=',')

    print(filePath)

    return dataFrame

def writeData(dataFrame,count):
    filePath = (
        DATA_PATH +
        'presence_weather_day_schedule_' +
        str(count) + 
        '.csv'
    )
    dataFrame.to_csv(filePath,index=False)

    print(filePath)

def combineWeatherData(count):
    MAX_SOLAR_POWER = 21000
    
    dfWeather = loadWeatherData()
    weatherColumns = dfWeather.columns
    weatherColumns = weatherColumns[1:]

    for i in range(count,count + 1):
        hour = 0
        hourBuffer = '00'
        dayBuffer = np.nan

        dfYear = loadPresenceData(i)

        debug = 0
        day = 0

        for minute in range(len(dfYear['time'])):
            if debug == 0:
                startTime = datetime.datetime.now()

            if dfYear['time'].iloc[minute][:2] != hourBuffer:
                hour = hour + 1
                hourBuffer = dfYear['time'].iloc[minute][:2]

            for column in weatherColumns:
                dfYear.at[minute,column] = dfWeather[column].iloc[hour]

            debug = debug + 1

            if debug == 1440:
                debug = 0
                endTime = datetime.datetime.now()

                print('task ' + str(day) + ' done ' + str(endTime - startTime))

                day = day + 1
        
        debug = 0
        day = 0

        for minute in range(len(dfYear['time'])):
            if debug == 0:
                startTime = datetime.datetime.now()

            if dfYear['date'].iloc[minute][8:] != dayBuffer:
                sunrise = 0
                sundown = 0
                dayBuffer = dfYear['date'].iloc[minute][8:]

                for j in range(minute,minute + 1439):
                    if (int(dfYear['is_day'].iloc[j]) == 1 and
                        sunrise == 0):
                            sunrise = j
                    elif (int(dfYear['is_day'].iloc[j]) == 0 and
                        sunrise != 0 and
                        sundown == 0):
                            sundown = j - 1
                            break

            if int(dfYear['is_day'].iloc[minute]) == 1:
                dfYear.at[minute,'solar power'] = (
                    np.round(MAX_SOLAR_POWER *
                    sineCurve(minute,sunrise,sundown) * 
                    (100 - 0.9 * int(dfYear['cloudcover'].iloc[minute])) / 100) + 
                    random.randrange(0,250)
                )
            else:
                dfYear.at[minute,'solar power'] = 0
            
            dfYear.at[minute,'used power'] = (
                400 + 
                40 *
                (int(dfYear['bathroom'].iloc[minute]) +
                int(dfYear['bedroom'].iloc[minute]) +
                int(dfYear['kitchen'].iloc[minute]) +
                int(dfYear['living room'].iloc[minute]) +
                int(dfYear['study'].iloc[minute])) +
                random.randrange(50)
            )

            debug = debug + 1

            if debug == 1440:
                debug = 0
                endTime = datetime.datetime.now()

                print('task ' + str(day) + ' done ' + str(endTime - startTime))

                day = day + 1

        writeData(dfYear,i)

def sineCurve(input,sunrise,sundown):
    midpoint = (sunrise + sundown) / 2
    halfPeriod = sundown - sunrise

    return np.sin(np.pi * (input - midpoint) / halfPeriod + np.pi / 2)