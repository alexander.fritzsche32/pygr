#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 17:38:00 2023

@author: alexander
"""

from path_constant import DATA_PATH
from data_base_functions import *

import datetime
import random

import numpy as np
import pandas as pd

from icecream import ic

def loadData(count):
    filePath = (
        DATA_PATH +
        'presence_weather_day_schedule_' +
        str(count) + 
        '.csv'
    )
    dataFrame = pd.read_csv(
        filePath,
        delimiter=',')

    print(filePath)

    return dataFrame

def writeData(dataFrame,count):
    filePath = (
        DATA_PATH +
        'presence_heating_day_schedule_' +
        str(count) + 
        '.csv'
    )
    dataFrame.to_csv(filePath,index=False)

    print(filePath)

def heatingCasino(count):
    MINUTES_PER_DAY = 24 * 60

    temperatureColumns = [
        'temperature boiler',
        'temperature living room',
        'temperature kitchen',
        'temperature bathroom',
        'temperature bedroom',
        'temperature study'
    ]

    heatingColumns = [
        'heating boiler',
        'heating living room',
        'heating kitchen',
        'heating bathroom',
        'heating bedroom',
        'heating study',
    ]

    rooms = [
        'boiler',
        'living room',
        'kitchen',
        'bathroom',
        'bedroom',
        'study'
    ]

    lastPresenceColumns = [
        'last at boiler',
        'last at living room',
        'last at kitchen',
        'last at bathroom',
        'last at bedroom',
        'last at study'
    ]

    minutesSinceLastPresence = [0,0,0,0,0,0]
    minutesSinceLastPresenceHome = 0

    day = 0

    for i in range(count,count + 1):
        dfDate = loadData(i)
        dfDate['boiler'] = 1
        
        debug = 0

        for minute in range(len(dfDate['time'])):
            if debug == 0:
                    startTime = datetime.datetime.now()

            solarPower = int(dfDate['solar power'].iloc[minute])
            usedPower = int(dfDate['used power'].iloc[minute])
            outsideTemperature = float(dfDate['temperature_2m'].iloc[minute])

            presence = []
            presencePlus5 = []

            for j in range(len(rooms)):
                presence.append(int(dfDate[rooms[j]].iloc[minute]))

                if minute + 5 < len(dfDate['time']):
                    presencePlus5.append(int(dfDate[rooms[j]].iloc[minute + 5]))
                else:
                    presencePlus5.append(presence)

                if presence[j] == 1:
                    minutesSinceLastPresence[j] = 0
                else:
                    minutesSinceLastPresence[j] = minutesSinceLastPresence[j] + 1

                dfDate.at[minute,lastPresenceColumns[j]] = minutesSinceLastPresence[j]
            
            minutesSinceLastPresenceHome = np.min(minutesSinceLastPresence[1:])
            dfDate.at[minute,'last at home'] = minutesSinceLastPresenceHome

            for j in range(len(temperatureColumns)):
                if temperatureColumns[j] == 'temperature boiler':
                    roomTemperature = (
                        np.round(15 +
                        random.randrange(0,700) / 
                        10,decimals=1)
                    )
                elif outsideTemperature >= 20:
                    roomTemperature = outsideTemperature
                else:
                    if outsideTemperature > 5:
                        roomTemperature = ( 
                            random.randrange(outsideTemperature * 10,211) / 
                            10
                        )
                    else:
                        roomTemperature = ( 
                            random.randrange(50,211) / 
                            10
                        )

                dfDate.at[minute,temperatureColumns[j]] = roomTemperature

                if (heatingColumns[j] == 'heating boiler' and
                    (roomTemperature < 35 or
                    (roomTemperature < 80 and
                    minutesSinceLastPresenceHome < MINUTES_PER_DAY and
                    (solarPower - usedPower > 3000)))):
                        dfDate.at[minute,heatingColumns[j]] = 1
                        usedPower = usedPower + 3000
                elif ((roomTemperature < outsideTemperature and
                    roomTemperature < 20 and
                    (presence[j] == 1 or
                    presencePlus5[j] == 1)) or
                    roomTemperature < 15 or
                    (roomTemperature < 17 and 
                    minutesSinceLastPresenceHome < MINUTES_PER_DAY) or
                    (roomTemperature < 20 and
                    minutesSinceLastPresenceHome < MINUTES_PER_DAY and
                    (solarPower - usedPower > 310))):
                        dfDate.at[minute,heatingColumns[j]] = 1
                        usedPower = usedPower + 300
                else:
                    dfDate.at[minute,heatingColumns[j]] = 0

                dfDate.at[minute,'used power'] = usedPower

            debug = debug + 1

            if debug == 1440:
                debug = 0
                endTime = datetime.datetime.now()

                print('task ' + str(day) + ' done ' + str(endTime - startTime))

                day = day + 1

        writeData(dfDate,i)