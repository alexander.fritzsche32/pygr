#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  5 17:33:12 2023

@author: alexander
"""

import datetime

def stringToDatetime(dateString):
    dateFormat = '%Y-%m-%d'
    
    return datetime.datetime.strptime(dateString,dateFormat)

def dateIsHoliday(dataFrame,index):
    if dataFrame['type'].iloc[index] == 'holiday':
        return True
    else:
        return False

def dateIsWork(dataFrame,index):
    if dataFrame['type'].iloc[index] == 'work':
        return True
    else:
        return False

def dateIsSick(dataFrame,index):
    if dataFrame['type'].iloc[index] == 'sick':
        return True
    else:
        return False

def dateIsOff(dataFrame,index):
    if dataFrame['type'].iloc[index] == 'off':
        return True
    else:
        return False

def dateIsVacation(dataFrame,index):
    if dataFrame['type'].iloc[index] == 'vacation':
        return True
    else:
        return False

def dateIsFree(dataFrame,index):
    return (dateIsVacation(dataFrame,index) or dateIsHoliday(dataFrame,index) or dateIsOff(dataFrame,index))